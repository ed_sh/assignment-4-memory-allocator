#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#include <unistd.h>


static void allocation_test(char* test_name, size_t heap_init_size, size_t first_mem, size_t second_mem, bool split_heap) {
    printf("--- Starting test %s ---\n", test_name);
    void* heap = heap_init(heap_init_size);
    assert(heap);
    debug_heap(stdout, heap);

    //making initialized memory somewhere after heap
    if(split_heap) assert(mmap( heap + heap_init_size*2, 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0 ) != MAP_FAILED);

    void* mem1 = _malloc(first_mem);
    assert(mem1);
    debug_heap(stdout, heap);
    printf("first memory allocated\n");

    if(second_mem) {
        void* mem2 = _malloc(second_mem);
        assert(mem2);
        debug_heap(stdout, heap);
        printf("second memory allocated\n");

        _free(mem2);
    }

    _free(mem1);
    heap_term();
    printf("all memory and heap freed\n"); 
    printf("--- Test %s ended successfully ---\n\n", test_name);
}

int main() {

    allocation_test("simple memory allocation and free", 1024, 512, 0, false);
    allocation_test("multiple memory allocation and free", 1024, 256, 256, false);
    allocation_test("heap growing", 256, 128, 1024, false);
    allocation_test("heap growing in new place", 256, 128, 2048, true);
    
    return 0;
}
